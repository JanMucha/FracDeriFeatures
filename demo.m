%% Initial clean up
close all;
clear all;
clc;

%% Paths and vars
% This example contains random handwriting signal with fs=150 Hz

load('handwriting_signal.mat');

%% Calculate Fractional order derivation based kinematic features
% This demo demonstrate different type of tool usage.
% IMPORTNAT! Period of the sampling lattice is by default set to h=7, 
%             becuase we mostly use wacom Tablets for signal aqusition. You
%             have to always set it by your signal properties!!
%        
%
%
% 1. Single input vector with default values for alpha and h.
%      
% 2. Single input vector with custom alpha and h.
%
% 3. Two paired input vecotrs with default values for alpha and h.
%
% 4. Two paired input vecotrs with custom alpha and h.

%% 1. Single input vector with default values for alpha and h.

XoutDefault = frac_deri_features(X); 

%% 2. Single input vector with custom alpha and h.

h = 8;
alpha = (0.1:0.1:1.5);

XoutCustom = frac_deri_features(X,alpha,h);


%% 3. Two paired input vecotrs with default values for alpha and h

XYoutDefault = frac_deri_features(X,Y);

%% 4. Two paired input vecotrs with custom alpha and h.

h = 8;
alpha = (0.1:0.1:1.5);

XYoutCustom = frac_deri_features(X,Y,alpha,h);


clear X Y h alpha;


%% EXAMPLE OF CONVERSION  TO MATRIX
% e.g. for velocity in 1. case
% each column represents different value of order alpha, ascending.

table = XoutCustom.velocity;
labels = fieldnames(table);
for i=1:length(labels)
    velocity_XoutDefault(:,i) = [table.(labels{i})];    
end

clear table labels i 
