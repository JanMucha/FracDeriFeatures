## FracDeriFeatures

  Computes fractional derivatives based features from input data using 
  the Grunwald-Letnikov estimation of order alpha for input function 
  on a regular gird with spacing h. As an input can be single vector 
  or two paired vectors (e.g. handwriting signal). In case of single 
  input vector velocity, acceleration and jerk are calculated for 
  selected alpha range. In case of two paired vectors input the same 
  kinematic features are provided included their 
  horizontal and vertical characteristics.

###  INPUT 

      x -> vector/function X
      y -> vector/function Y
      alpha -> alpha order of fractional derivatives 
           DEFAULT  alpha = (0.1:0.1:1)
      h -> period of the sampling lattice
           DEFAULT h=7;
      
  
###  OUTPUT
      out -> structure with calculated basic kinematic features, 
             where number of column represents alpha order.
      SINGLE VECTOR INPUT
      out.velocity                = velocity for each alpha
      out.acceleration            = acceleration for each alpha
      out.jerk                    = jerk for each alpha

      TWO VECTOR INPUT
      out.velocity                = velocity for each alpha
      out.acceleration            = acceleration for each alpha
      out.jerk                    = jerk for each alpha
      out.vert_velocity           = vertical velocity for each alpha
      out.vert_acceleration       = vertical acceleration for each alpha
      out.vert_jerk               = vertical jerk for each alpha
      out.horz_velocity           = horizontal velocity for each alpha
      out.horz_acceleration       = horizontal acceleration for each alpha
      out.horz_jerk               = horizontal jerk for each alpha

###  USAGE
      out = frac_deri_features(x); 
              Copmute basic kinematic features for single vector input, 
              when default values for alpha and h will be used.

      out = frac_deri_features(x,alpha,h);
              Copmute basic kinematic features for single vector input, 
              with alpha and h set by user.

      out = frac_deri_features(x,y);
              Copmute basic kinematic features for two vector input, 
              when default values for alpha and h will be used.

      out = frac_deri_features(x,y,alpha,h);
              Copmute basic kinematic features for two vector input, 
              with alpha and h set by user.


Copyright 2018 Jan Mucha


In case of problems/questions contact muchajano@phd.feec.vutbr.cz. 

