function out = frac_deri_features(x,y,alpha,h)
%% FracDeriFeatures
%
%   Computes fractional derivatives based features from input data using 
%   the Grunwald-Letnikov estimation of order alpha for input function 
%   on a regular gird with spacing h. As an input can be single vector 
%   or two paired vectors (e.g. handwriting signal). In case of single 
%   input vector velocity, acceleration and jerk are calculated for 
%   selected alpha range. In case of two paired vectors input the same 
%   kinematic features are provided included their 
%   horizontal and vertical characteristics.
%
%   INPUT 
%
%       x -> vector/function X
%       y -> vector/function Y
%   alpha -> alpha order of fractional derivatives 
%            DEFAULT  alpha = (0.1:0.1:1)
%       h -> period of the sampling lattice
%            DEFAULT h=7;
%       
%   
%   OUTPUT
%       out -> structure with calculated basic kinematic features, 
%              where number of column represents alpha order.
%       SINGLE VECTOR INPUT
%       out.velocity                = velocity for each alpha
%       out.acceleration            = acceleration for each alpha
%       out.jerk                    = jerk for each alpha
%
%       TWO VECTOR INPUT
%       out.velocity                = velocity for each alpha
%       out.acceleration            = acceleration for each alpha
%       out.jerk                    = jerk for each alpha
%       out.vert_velocity           = vertical velocity for each alpha
%       out.vert_acceleration       = vertical acceleration for each alpha
%       out.vert_jerk               = vertical jerk for each alpha
%       out.horz_velocity           = horizontal velocity for each alpha
%       out.horz_acceleration       = horizontal acceleration for each alpha
%       out.horz_jerk               = horizontal jerk for each alpha
%
%   USAGE
%       out = frac_deri_features(x); 
%               Copmute basic kinematic features for single vector input, 
%               when default values for alpha and h will be used.
%
%       out = frac_deri_features(x,alpha,h);
%               Copmute basic kinematic features for single vector input, 
%               with alpha and h set by user.
%
%       out = frac_deri_features(x,y);
%               Copmute basic kinematic features for two vector input, 
%               when default values for alpha and h will be used.
%
%       out = frac_deri_features(x,y,alpha,h);
%               Copmute basic kinematic features for two vector input, 
%               with alpha and h set by user.
%
%   Copyright 2018 Jan Mucha
%   Contact: muchajano@phd.feec.vutbr.cz 
%
%% Check input 
TwoDsignal=false;

%Input only x; set default values
if nargin == 1   
    if(length(x) < 2 || isnumeric(x)==0)
        error("Wrong input argument. Input must be not null and numeric. Check input and try it again!");
    else
        alpha   = (0.1:0.1:1);
        h       = 7; 
    end
    
%Input x and y; set default values    
elseif nargin == 2
    if(length(x) < 2 || length(y) < 2 || isnumeric(x)==0 || isnumeric(y)==0)
        error("Wrong input arguments. Input must be two numeric vectors! Check input and try it again!");        
    else
        alpha   = (0.1:0.1:1);
        h       = 7;
        TwoDsignal = true;
    end
    

%Input x + alpha and h    
elseif nargin == 3
    % Shift input accroding to unused y
    h = alpha;
    alpha = y;
    y = [];
    if(length(x) < 2 || isnumeric(x)==0 || isnumeric(alpha)==0 || isnumeric(h)==0)
        error("Wrong input arguments. Input must be one numeric vector. Alpha and h must be set ot null ! Check input and try it again!");        
    end
    
%Input x and y + alpha and h        
elseif nargin > 3
    if (length(y) > 1)
       TwoDsignal = true;
       
       if(length(x) < 2 || length(y) < 2 || isnumeric(x)==0 || isnumeric(y)==0 ...
                                        || isnumeric(alpha)==0 || isnumeric(h)==0)
            error("Wrong input arguments. Input must be two numeric vectors. Alpha and h must be set not null! Check input and try it again!");
       end
    end
end


%% Calculate fractional derivatives based features for single input
if(TwoDsignal == false)
    
    %calc velocity from length for each alpha
    for i=1:length(alpha)       
        fieldname = join(['alpha_',strrep(num2str(alpha(i)),'.','_')]);
        out.velocity.(fieldname) = fgl_deriv( alpha(i),x, h );        
    end
    
    %calc acceleration for each alpha
    for i=1:length(alpha)
        fieldname = join(['alpha_',strrep(num2str(alpha(i)),'.','_')]);
        out.acceleration.(fieldname) = fgl_deriv( alpha(i),out.velocity.alpha_1, h );
    end
    
    %calc jerk for each alpha
    for i=1:length(alpha)
        fieldname = join(['alpha_',strrep(num2str(alpha(i)),'.','_')]);        
        out.jerk.(fieldname) = fgl_deriv( alpha(i),out.acceleration.alpha_1, h );
    end
end


%% Calculate fractional derivatives based features for 2D input
if(TwoDsignal)
    
    l = sqrt((x(2:end)-x(1:end-1)).^2 + (y(2:end)-y(1:end-1)).^2);
    
    %calc velocity from length for each alpha
    for i=1:length(alpha)
        fieldname = join(['alpha_',strrep(num2str(alpha(i)),'.','_')]);
        out.velocity.(fieldname) = fgl_deriv( alpha(i),l, h );        
    end    
    %calc acceleration for each alpha
    for i=1:length(alpha)
        fieldname = join(['alpha_',strrep(num2str(alpha(i)),'.','_')]);
        out.acceleration.(fieldname) = fgl_deriv( alpha(i),out.velocity.alpha_1, h );
    end
    %calc jerk for each alpha   
    for i=1:length(alpha)
        fieldname = join(['alpha_',strrep(num2str(alpha(i)),'.','_')]);
        out.jerk.(fieldname) = fgl_deriv( alpha(i),out.acceleration.alpha_1, h );
    end
    
   % Calculate the vertical velocity, acceleration and jerk
    for i=1:length(alpha)
        fieldname = join(['alpha_',strrep(num2str(alpha(i)),'.','_')]);
        out.vert_velocity.(fieldname) = fgl_deriv( alpha(i),y, h );
    end
    
    for i=1:length(alpha)
        fieldname = join(['alpha_',strrep(num2str(alpha(i)),'.','_')]);
        out.vert_acceleration.(fieldname) = fgl_deriv( alpha(i),out.vert_velocity.alpha_1, h );
    end
    
    for i=1:length(alpha)
        fieldname = join(['alpha_',strrep(num2str(alpha(i)),'.','_')]);
        out.vert_jerk.(fieldname) = fgl_deriv( alpha(i),out.vert_acceleration.alpha_1, h );
    end
    
    % Calculate the horizontal velocity, acceleration and jerk
         
    for i=1:length(alpha)
        fieldname = join(['alpha_',strrep(num2str(alpha(i)),'.','_')]);
        out.horz_velocity.(fieldname) = fgl_deriv( alpha(i),x, h );
    end
    
    for i=1:length(alpha)
        fieldname = join(['alpha_',strrep(num2str(alpha(i)),'.','_')]);
        out.horz_acceleration.(fieldname) = fgl_deriv( alpha(i),out.horz_velocity.alpha_1, h );
    end

    for i=1:length(alpha)
        fieldname = join(['alpha_',strrep(num2str(alpha(i)),'.','_')]);
        out.horz_jerk.(fieldname) = fgl_deriv( alpha(i),out.horz_acceleration.alpha_1, h );
    end    
    
end

